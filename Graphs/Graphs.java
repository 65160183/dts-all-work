class Graph {
    private final int MAX_VERTS = 20;
    private Vertex vertexList[];
    private int adjMat[][];
    private int nVerts;
    private StackX theStack;

    public Graph() {
        vertexList = new Vertex[MAX_VERTS];
        adjMat = new int[MAX_VERTS][MAX_VERTS];
        nVerts = 0;

        for (int j = 0; j < MAX_VERTS; j++) {
            for (int k = 0; k < MAX_VERTS; k++) {
                adjMat[j][k] = 0;
            }
        }

        theStack = new StackX();
    }

    public void addVertex(char lab) {
        vertexList[nVerts++] = new Vertex(lab);
    }

    public void addEdge(int start, int end) {
        adjMat[start][end] = 1;
        adjMat[end][start] = 1;
    }

    public void displayVertex(int v) {
        System.out.print(vertexList[v].label);
    }

    public void dfs() {
        // Begin at vertex 0
        vertexList[0].wasVisited = true; // Mark the starting vertex as visited
        displayVertex(0); // Display the starting vertex
        theStack.push(0); // Push the starting vertex onto the stack

        while (!theStack.isEmpty()) {
            // Get an unvisited vertex adjacent to the top of the stack
            int v = getAdjUnvisitedVertex(theStack.peek());

            if (v == -1) {
                // If no such vertex, pop the current one
                theStack.pop();
            } else {
                // If an unvisited adjacent vertex exists, mark it, display it, and push it
                vertexList[v].wasVisited = true; // Mark it as visited
                displayVertex(v); // Display the vertex
                theStack.push(v); // Push the vertex onto the stack
            }
        }

        // Stack is empty, so we're done. Reset all visited flags.
        for (int j = 0; j < nVerts; j++) {
            vertexList[j].wasVisited = false;
        }
    }

    public int getAdjUnvisitedVertex(int v) {
        for (int j = 0; j < nVerts; j++) {
            if (adjMat[v][j] == 1 && !vertexList[j].wasVisited) {
                return j; // Return the first unvisited adjacent vertex
            }
        }
        return -1; // No such unvisited vertices
    }
    

}
