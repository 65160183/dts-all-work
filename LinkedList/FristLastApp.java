// FirstLastApp.java
public class FristLastApp {
    public static void main(String[] args) {
        FirstLastList theList = new FirstLastList(); // make a new list
        theList.insertFirst(22); // insert at the front
        theList.insertFirst(44);
        theList.insertFirst(66);
        theList.insertLast(11); // insert at the rear
        theList.insertLast(33);
        theList.insertLast(55);
        theList.displayList(); // display the list
        theList.deleteFirst(); // delete the first two items
        theList.deleteFirst();
        theList.displayList(); // display again
    }
}